import { 
    query_pokedex,
    query_pokedex_id,query_pokedex_bypk,
    query_pokedex_description,
    query_pokedex_description_bypk,
    query_pokedex_name
} from '../querys/pokedex'

Cypress.Commands.add("pokedex",(asc, limit) => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query_pokedex(asc,limit)
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})

Cypress.Commands.add("pokedex_id",(id) => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query_pokedex_id(id)
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})

Cypress.Commands.add("pokedex_bypk",(id) => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query_pokedex_bypk(id)
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})

Cypress.Commands.add("pokedex_description",() => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query_pokedex_description()
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})

Cypress.Commands.add("pokedex_description_bypk",(id) => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query_pokedex_description_bypk(id)
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})

Cypress.Commands.add("pokedex_name",() => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query_pokedex_name()
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})