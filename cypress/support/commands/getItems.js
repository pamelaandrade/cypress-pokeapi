const query = require('../querys/getItems')

Cypress.Commands.add("getItems",() => {
    cy.request({
        method: 'POST',
        url: '/graphql/v1beta',
        headers: {
            'Content-Type': 'application/json',
            'accept': '*/*'
        },
        body: JSON.stringify({
            query: query()
        })
    }).then( response => {
        expect(response.status).to.eq(200)
    })
})