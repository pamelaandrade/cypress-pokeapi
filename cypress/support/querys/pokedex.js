
function query_pokedex (ordem, limit){
    return `
    query samplePokeAPIquery {
        pokemon_v2_pokedex(order_by: {name: ${ordem}}, limit: ${limit}) {
          name
          id
          is_main_series
          pokemon_v2_region {
            id
            name
            pokemon_v2_generations {
              id
              name
              pokemon_v2_abilities {
                generation_id
                id
                is_main_series
                name
              }     
            }
          }
        }
      }
    `    
}

function query_pokedex_id(id){
    return`
    query samplePokeAPIquery {
        pokemon_v2_pokedex(where: {id: {_eq: ${id}}}) {
          name
          id
          is_main_series
          pokemon_v2_pokedexdescriptions {
            description
            id
            language_id
            pokedex_id
          }
          region_id
          pokemon_v2_pokedexnames {
            id
            language_id
            name
            pokedex_id
          }
        }
      }
      
    `
}

function query_pokedex_bypk(id){
  return`
  query samplePokeAPIquery {
    pokemon_v2_pokedex_by_pk(id: ${id}) {
      name
      is_main_series
      id
    }
  }
  `
}

function query_pokedex_description () {
  return `
    query samplePokeAPIquery {
      pokemon_v2_pokedexdescription {
        description
        id
        language_id
        pokedex_id
        pokemon_v2_language {
          id
          name
          official
          order
        }
      }
    }
  `
}

function query_pokedex_description_bypk(id){
  return`
  query samplePokeAPIquery {
    pokemon_v2_pokedexdescription_by_pk(id: ${id}) {
      language_id
      id
      description
      pokedex_id
    }
  }
  `
}

function query_pokedex_name(){
  return `
  query samplePokeAPIquery {
    pokemon_v2_pokedexname {
      id
      language_id
      name
      pokedex_id
    }
  }
  
  `
}

module.exports={
    query_pokedex,
    query_pokedex_id,
    query_pokedex_bypk,
    query_pokedex_description,
    query_pokedex_description_bypk,
    query_pokedex_name
}
