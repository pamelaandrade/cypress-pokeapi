# Caso de Caso de Estudo cypress-gitlab Pokeapi graphql + Nyan Report

## **_Configuração do Projeto_**

- Clonar o repositório `https://gitlab.com/pamelaandrade/cypress-pipeline.git`
<br>

## **_Tecnologias Utilizadas_**

- Cypress ^7.2.0
- Mocha
- PokeAPi
- Nyan Report
<br>

## **_Cypress_**

No arquivo de configuração do cyress `cypress.json`, contém o link para utilizar o graphql do Pokeapi, em `baseURL`.
Em `reporter`, está configurado o Nyan report, que é ativado a cada execução.

```
{
  "defaultCommandTimeout": 60000,
  "requestTimeout": 60000,
  "responseTimeout": 60000,
  "viewportWidth": 1366,
  "viewportHeight": 768,
  "watchForFileChanges": true,
  "baseUrl": "https://beta.pokeapi.co",
  "video": false,
  "reporter": "nyan"
}

```
<br>

## **_Instalar Dependências_**

Executar o comando:

```
    npm i
```

<br>

## **_Arquitetura_**

```
    ├── cypress
        ├── fixtures: padrão da instalação.
        ├── integration: onde está os cenários automatizados.
        ├── plugins: padrão da instalação.
        ├── support
            ├── querys: todos as querys do GraphQL
            ├── commands: todos os commmands de execução da API.
            ├── commands.js: padrão da instalação.
            ├── index.js: necessário importar todos os arquivos da pasta commands.
        ├── cyress.json
        ├── packege-lock.json
        ├── yarn.lock
        ├── README.md
```
<br>

## **_Rodar os testes locais_**

* Para abrir o cypress, utilizar o comando:

``` 
    npx cypress open
```

* Para rodar todos os testes, utilizar o comando:

``` 
    npx cypress run
```
<br>

## **_Links de dependências_**

* PokeApi: https://pokeapi.co/docs/graphql
* Nyan Report: https://mochajs.org#reporters
